//2.Write a Java program to extract date, time from the date string
package com.assignment7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class Q2 {
	public static void main(String[] args) throws ParseException {
		String datestring = "2021-03-13 11:00:10";
	      Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datestring);
	      String str = new SimpleDateFormat("MM/dd/yyyy, H:mm:ss").format(date);
	       System.out.println(str);
	}

}
