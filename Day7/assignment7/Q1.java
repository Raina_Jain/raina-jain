//1.Write a java program to print current date and time in the specified format.
package com.assignment7;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Q1 {

	public static void main(String[] args) {
		Date date=new Date();
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(sdf.format(date));

	}

}
