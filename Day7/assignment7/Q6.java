/*6.Include a class�UserMainCode�with a static method �getNumberOfDays� that accepts 2
integers as arguments and returns an integer. The first argument corresponds to the year and
the second argument corresponds to the month code. The method returns an integer
corresponding to the number of days in the month.�
Create a class Main which would get 2 integers as input and call the static
method�getNumberOfDays�present in the�UserMainCode.�
Input and Output Format:�
Input consists of 2 integers that correspond to the year and month code.�
Output consists of an integer that correspond to the number of days in the month in the
given year.�
Sample Input:�
2000�
1�
Sample Output:�
29*/
package com.assignment7;
import java.util.*;
class UserMainCode{
	public static int getNumberOfDays(int year,int month) {
		int number_Of_DaysInMonth = 0; 
        String MonthOfName = "Unknown"; 
		switch (month) {
         case 1:
             MonthOfName = "January";
             month = 31;
             break;
         case 2:
             MonthOfName = "February";
             if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
            	 month = 29;
             } else {
            	 month = 28;
             }
             break;
         case 3:
             MonthOfName = "March";
             month = 31;
             break;
         case 4:
             MonthOfName = "April";
             month = 30;
             break;
         case 5:
             MonthOfName = "May";
             month = 31;
             break;
         case 6:
             MonthOfName = "June";
             month = 30;
             break;
         case 7:
             MonthOfName = "July";
             month = 31;
             break;
         case 8:
             MonthOfName = "August";
             month = 31;
             break;
         case 9:
             MonthOfName = "September";
             month = 30;
             break;
         case 10:
             MonthOfName = "October";
             month = 31;
             break;
         case 11:
             MonthOfName = "November";
             month = 30;
             break;
         case 12:
             MonthOfName = "December";
             month = 31;
     }
     
	return month;
	
	}
	 
}
public class Q6 {

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		System.out.println("enter month");
		
        int month = in.nextInt();

        System.out.println("enter year ");
        int year = in.nextInt();
        UserMainCode q=new UserMainCode();
       
        System.out.println( q.getNumberOfDays(year, month)  );

	}

}
