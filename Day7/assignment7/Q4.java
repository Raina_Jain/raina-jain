//4.Write a Java program to get the maximum value of the year, month, week, date from the
//current date of a default calendar
package com.assignment7;

import java.util.Calendar;

public class Q4 {

	public static void main(String[] args) {
		Calendar cal= Calendar.getInstance();
		System.out.println("the current date is"+" "+cal.getTime());
        
        System.out.println(cal.getActualMaximum(Calendar.MONTH));
        System.out.println(cal.getActualMaximum(Calendar.WEEK_OF_YEAR));
        System.out.println(cal.getActualMaximum(Calendar.DATE));
        System.out.println(cal.getActualMaximum(Calendar.YEAR));
	}

}
