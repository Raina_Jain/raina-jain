package com.assignment7;

 import java.io.IOException;
import java.text.*;
import java.util.*;
public class Q13 {
public static void main(String[] args) throws ParseException, IOException {
             Scanner sc=new Scanner(System.in);
             String s1 = sc.nextLine();
             String s2 = sc.nextLine();
             
                System.out.println(getDateDiff(s1,s2));
}
                public static int getDateDiff(String s1, String s2) throws ParseException {
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                                Date d1=sdf.parse(s1);
                                Date d2=sdf.parse(s2);
                Calendar cal=Calendar.getInstance();
               
                cal.setTime(d1);
                int months1=cal.get(Calendar.MONTH);
                int year1=cal.get(Calendar.YEAR);
                int day1=cal.get(Calendar.DAY_OF_MONTH);
                
                cal.setTime(d2);
                int months2=cal.get(Calendar.MONTH);
                int year2=cal.get(Calendar.YEAR);
                int day2=cal.get(Calendar.DAY_OF_MONTH);
                int n=((year2-year1)*12)+(months2-months1)+(day2-day1);
                return n;
}
}

