package com.assignment7;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.*;


public class Q10
{
	public static String getvalues1(String s1,String s2) {
		if(s1.matches("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}") && s1.matches("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}"))
		{
			SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat sdf1=new SimpleDateFormat("MM/dd/yyyy");
			try{
				Date d1=sdf.parse(s1);
				Date d2=sdf.parse(s2);
				Calendar cal=Calendar.getInstance();
				cal.setTime(d1);
				long y=cal.getTimeInMillis();
				cal.setTime(d2);
				long y1=cal.getTimeInMillis();
				String s3=sdf1.format(d1);
				String s4=sdf1.format(d2);
				if(y<y1)return s3;
				else
					return s4;
				}
			catch(ParseException e){
				return"invalid";
				}
			}
		else
			return"invalid";
		}
	public static void  main(String[] args) {
		Scanner in=new Scanner(System.in);
		String s1=in.next();
		String s2=in.next();
		System.out.println(Q10.getvalues1(s1,s2));
		}
	}
