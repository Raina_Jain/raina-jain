package com.assignment7;

public class UserMainCodeCheckSumOfODD {
   public static int checkSum(int n) {
	   int sum=0;
	    while(n>0){
	      int rem = n%10;
	      if(rem%2!=0){
	        sum = sum+rem;
	      }
	      n = n/10;
	    }
	    
	    if(sum%2==0){
	      System.out.println("Sum of odd digits is even");
	    }else{
	      System.out.println("Sum of odd digits is odd");
	    }
	    return 0;
   }
}


