package assignment6;
import java.util.Date;
public class ThreadTimeSleep extends Thread {
	private String threadName;
	ThreadTimeSleep(String name){
		threadName=name;
		System.out.println(threadName);
	}
	static void dateTime() {
		Date date=new Date();
		String s=String.format("current date is:"+" "+date);
		System.out.println(s);
	}
	public void run() {
		System.out.println(threadName);
		try {
			for(int i=0;i<2;i++) {
				dateTime();
				Thread.sleep(1000);
			}
		}
		catch(InterruptedException e) {
			System.out.println("thread"+ threadName +"interrupted");
			
		}
	}

}

