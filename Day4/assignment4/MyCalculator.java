package com.assignment4;
import java.util.Scanner;
public class MyCalculator implements AdvancedArithmetic{
public int divisor_sum(int a) {
	int sum=0;
	for(int i=1;i<a;i++){
		if(a%i==0) {
			sum=sum+i;
		}
	}
	return sum;
}

public static void main(String[] args) {
	MyCalculator m=new MyCalculator();
	Scanner sc=new Scanner(System.in);
	int a=sc.nextInt();
	System.out.println(m.divisor_sum(a));
	sc.close();
}
}

