package com.assignment4;

public class Rectangle extends Shape{
	 private Integer length,width;
	 public Integer getLength() {
		 return length;
	 }
	 public void setLength(Integer length) {
		 this.length=length;
		 
	 }
	 public Integer getWidth() {
		 return width;
	 }
	 public void setWidth(int width) {
		 this.width=width;
		 
	 }
	 public float calculateArea() {
		 Float area=Float.valueOf((float)length*width);
		 return area;
	 }
	 public Rectangle(String name,Integer length,Integer width) {
		 super(name);
		 this.length=length;
		 this.width=width;
	 }
}