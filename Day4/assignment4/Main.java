package com.assignment4;
import java.util.Scanner;



public class Main {

	public static void main(String[] args) {
		System.out.println("Select the card\n1.Membership Card\n2.Payback Card");
		Scanner sc=new Scanner(System.in);
		int choice=sc.nextInt();
		
		if (choice==1) {
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			System.out.println("Enter the points in card");
			Integer rating=Integer.valueOf(sc.nextInt());
			String[] strarr=info.split("\\|");
			MembershipCard mc=new MembershipCard(strarr[0],strarr[1],strarr[2],rating);
			System.out.println(mc.getholderName()+"'s Membership Card Details");
			System.out.println("Card Number "+mc.getcardNumber());
			System.out.println("Expiry Date "+mc.getexpiryDate());
			System.out.println("Rating "+mc.getrating());
			
		}
		
		else if(choice==2) {
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			String[] strarr=info.split("\\|");
			System.out.println("Enter points in card");
			Integer points=Integer.valueOf(sc.nextInt());
			System.out.println("Enter the total amount");
			Double totalAmount=Double.valueOf(sc.nextDouble());
	        PaybackCard pc=new PaybackCard(strarr[0], strarr[1], strarr[2], points, totalAmount);
	        System.out.println(pc.getholderName()+"'s Payback Card Details:\n"+"Card Number "+pc.getcardNumber());
	        System.out.println("Expiry Date "+pc.getexpiryDate());
	        System.out.println("Points Earned "+pc.getpointsEarned());
	        System.out.println("Total Amount "+pc.gettotalAmount());
		}
		sc.close();

	}

}
