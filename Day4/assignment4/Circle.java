package com.assignment4;

public class Circle extends Shape{
	private int radius;
	 public Circle(String name,Integer radius){
		super(name);
		this.radius=radius;
	}
	 public Integer getradius() {
		 return radius;
	 }
	 public void setradius(int radius) {
		 this.radius=radius;
	 }
	 public float calculateArea() {
		float area=(radius*radius*3.14f);
		
		 
		 return area;
	 }
}
