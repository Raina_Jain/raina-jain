package com.assignment4;
import java.util.Scanner;



 

 
public class Ex1{

	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter shape name");
	System.out.println("1.circle\n2.Rectangle\n3.Square");
	String choice=sc.next();
	if(choice.equals("circle")) {
		System.out.println("enter radius");
		int rad=sc.nextInt();
		Circle c1=new Circle("c1",rad);
		System.out.printf("%.2f",c1.calculateArea());
	}
	else if(choice.equals("Rectangle")) {
		System.out.println("Enter the length");
		int length=sc.nextInt();
		System.out.println("Enter the width");
		int width=sc.nextInt();
		Rectangle r1=new Rectangle("R1",length,width);
		System.out.printf("%.2f",r1.calculateArea());
	}
	else if(choice.equals("Square")) {
		System.out.println("Enter the side");
		int side=sc.nextInt();
		Square sq1=new Square("Sq1",side);
		System.out.printf("%.2f",sq1.calculateArea());
	}
	}

}
