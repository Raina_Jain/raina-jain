package com.assignment4;

public class Square extends Shape{
	 private int side;
	 public int getSide() {
		 return side;
		
	 }
	 public void setSide(int side) {
		 this.side=side;
	 }
	 public float calculateArea() {
		 float area=Float.valueOf((float)side*side);
	     return area;
	 }
	 public Square(String name,int side) {
		 super(name);
		 this.side=side;
	 }
	 
}
