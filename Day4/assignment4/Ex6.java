package com.assignment4;

public class Ex6 {
static int a=555;
public static void main(String[] args) {
	A2 objA=new A2();
	B objB1=new B();
	A2 objB2=new B();
	C objC1=new C();
	B objC2=new C();
	A2 objC3=new C();
	objA.display();
	objB1.display();
	objB2.display();
	objC1.display();
	objC2.display();
	objC3.display();
}
}

//OUTPUT:
//a in A=100
//b in B=100
//b in B=100
//c in C=300
//c in C=300
//c in C=300