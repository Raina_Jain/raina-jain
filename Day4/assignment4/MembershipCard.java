package com.assignment4;

class MembershipCard extends Card{
	private Integer rating;
	public Integer getrating() {
		return rating;
	}
	public void set(Integer rating) {
		this.rating=rating;
	}
	public MembershipCard(String holderName,String cardNumber,String expiryDate,Integer rating) {
		super(holderName,cardNumber,expiryDate);
		this.rating=rating;
	}

	public MembershipCard() {
		
	}
}	


