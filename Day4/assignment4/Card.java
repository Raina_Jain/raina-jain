package com.assignment4;

public abstract class Card{
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	
	public String getholderName() {
		return holderName;
		
	}
	public void setName(String holderName) {
		this.holderName=holderName;
}
	public String getcardNumber() {
		return cardNumber;
		
	}
	public void setcardNumber(String cardNumber) {
		this.cardNumber=cardNumber;
}
	public String getexpiryDate() {
		return expiryDate;
		
	}
	public void setexpiryDate(String expiryDate) {
		this.expiryDate=expiryDate;
}
	public Card(String holderName,String cardNumber,String expiryDate){
		this.holderName=holderName;
		this.cardNumber=cardNumber;
		this.expiryDate=expiryDate;
	}
	public Card() {
		
	}
}