package com.assignment4;

class PaybackCard extends Card{
	private Integer pointsEarned;
	private Double totalAmount;
	public Integer getpointsEarned() {
		return pointsEarned;
	}
	public void set(Integer pointsEarned) {
		this.pointsEarned=pointsEarned;
	}
	public Double gettotalAmount() {
		return  totalAmount;
	}
	public void set(Double  totalAmount) {
		this. totalAmount= totalAmount;
	}
	public PaybackCard() {
		
	}
	public PaybackCard(String holderName,String cardNumber,String expiryDate,Integer pointsEarned,Double totalAmount) {
		super(holderName,cardNumber,expiryDate);
		this.pointsEarned=pointsEarned;
		this.totalAmount=totalAmount;
	}
	
}
