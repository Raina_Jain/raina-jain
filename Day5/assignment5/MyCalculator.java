package com.assignment5;
import java.util.*;
import java.io.*;

 class MyCalculator {
	 
	 long power(int n,int p)throws Exception
	 {
		 
		 if(n<0||p<0) {
			 throw new Exception("n and p should be positive");
		 }
		 if(n==0||p==0)
		 {
			 throw new Exception("n and p should not be zero");
		 }
		 long a=(long)Math.pow(n,p);
		 return a;
	 }

}

