Joins
1.Write a query in SQL to display the first name, last name, department
number, and department name for each employee
mysql> select e.first_name,d.department_id,d.department_name from employees e   join departments d on e.department_id=d.department_id;
+------------+---------------+-----------------+
| first_name | department_id | department_name |
+------------+---------------+-----------------+
| valli      |            60 | it              |
| david      |            60 | it              |
| bruce      |            60 | it              |
| alexander  |            60 | it              |
| lex        |            90 | executive       |
| neena      |            90 | executive       |
| steven     |            90 | executive       |
| william    |           110 | accounting      |
| shelly     |           110 | accounting      |
| diana      |           110 | accounting      |
+------------+---------------+-----------------+
2.Write a query in SQL to display the first name, last name, department
number and department name, for all employees for departments 80 or
40
 select first_name,d.department_id from employees e,departments d where e.department_id=d.department_id and d.department_id in(80,40);
Empty set (0.69 sec)

3.Write a query in SQL to display the first name of all employees
including the first name of their manager
mysql> select e.first_name as employee_name,m.first_name as manager_name from employees e join departments d on e.employee_id=d.manger_id join employees m on d.department_id=m.department_id;
+---------------+--------------+
| employee_name | manager_name |
+---------------+--------------+
| steven        | steven       |
| steven        | neena        |
| steven        | lex          |
| alexander     | alexander    |
| alexander     | bruce        |
| alexander     | david        |
| alexander     | valli        |
| shelly        | diana        |
| shelly        | shelly       |
| shelly        | william      |
+---------------+--------------+
10 rows in set (0.00 sec)
4.Write a query in SQL to display all departments including those where
does not have any employee
mysql> select distinct d.department_id from departments d   left outer  join employees e on e.department_id=d.department_id  ;
+---------------+
| department_id |
+---------------+
|            10 |
|            20 |
|            30 |
|            40 |
|            50 |
|            60 |
|            70 |
|            80 |
|            90 |
|           100 |
|           110 |
+---------------+
5.Write a query in SQL to display the first name, last name, department
number and name, for all employees who have or have not any
department


mysql> select e.first_name,d.department_id,department_name from employees e left outer join departments d on e.department_id=d.department_id;
+------------+---------------+-----------------+
| first_name | department_id | department_name |
+------------+---------------+-----------------+
| steven     |            90 | executive       |
| neena      |            90 | executive       |
| lex        |            90 | executive       |
| alexander  |            60 | it              |
| bruce      |            60 | it              |
| david      |            60 | it              |
| valli      |            60 | it              |
| diana      |           110 | accounting      |
| shelly     |           110 | accounting      |
| william    |           110 | accounting      |
+------------+---------------+-----------------+
10 rows in set (0.01 sec)

jdbc:


1.Let�s write code to insert a new record into the table Users with
following details:

o username: steve
o password: secretpass
o fullname: steve paul
o email: steve.paul@hcl.com
2.�Select all records from the Users table and print out details for each
record.
3.�Write a code to update all the details� of �steve paul�.
4. Write a code to delete a record whose username field contains
�steve�.
package com.pack;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class PracticeMysql {
public static void main (String[] args)throws Exception {
	Connection con=null;
	Statement st=null;
	try {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter username");
		String uname=sc.nextLine();
		System.out.println("enter password");
		String p=sc.nextLine();
		System.out.println("enter fullname");
		String fname=sc.nextLine();
		System.out.println("enter email");
		String email=sc.nextLine();
		con=DBConnection.getConnection();
		st=con.createStatement();
		st.executeUpdate("insert into Users values(' "+uname+" ',' "+p+" ',' "+fname+" ',' "+email+" ')");
		String sql="update  users set (uname,p,fname,email) where email='steve@gmail.com' ";
		 int nr=st.executeUpdate(sql);
		 
		 System.out.println(nr+"rows effected");
		 String sql1="delete from  users where uname='steve' ";
		 int nr1=st.executeUpdate(sql1);
		 
		 System.out.println(nr1+"rows effected");
         ResultSet r=st.executeQuery("select * from users");
		while(r.next()) {
			System.out.println(r.getString("username")+" "+r.getString("password")+" "+r.getString("fullname")+" "+r.getString("email"));
			
		}
	}
	catch(Exception e) {
		System.out.println(e);
	}
	finally {
		st.close();
		con.close();
	}
	
}
}

Jdbc Transaction Management
1.Create a table named customer including name,salary,email.
2.One of the constraints on the table is that email has to be unique. If we
enter same email a second time to violate this constraint. It results in
SQL exception. Have to� rollback transaction programmatically in
exception handling block.


mysql->create table customer(c_name varchar(20),c_salary varchar(20),c_email varchar(20) unique);

public class Rollbackdemo{
public static void main(String[] args){
try{
Class.forName();
Connection con =DriverManager.getConnection();
con.setAutoCommit(false);
Statement st=con.createStatement();
st.executUpdate("insert into customer values("raina",20000,"rainajain@hcl.com"));
con.commit();
}
catch(SQLException e)
{
con.rollback();
System.out.println("Email already exists");
}
}


