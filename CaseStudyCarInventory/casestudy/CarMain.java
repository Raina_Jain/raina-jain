package com.casestudy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;
import com.pack.DBConnection;
public class CarMain {
	Scanner sc=new Scanner(System.in);
     public static void main(String[] args) throws Exception {
		Scanner sc=new Scanner(System.in);
		CarMain car=new CarMain();
		int ch=0;
		
		do {
			try {
            System.out.println("Welcome to Guntur Used Car Inventory");
			System.out.println("Enter Your Choice");
			System.out.println("1. To Add a new Car");
			System.out.println("2. To List");
			System.out.println("3. To Delete a Car detail");
			System.out.println("4. To Update the Car details");
			System.out.println("5. Exit");
			ch=Integer.parseInt(sc.nextLine());
			switch(ch) {
			case 1:
				car.addData();
				break;
			case 2:
				car.listData();
				break;
			case 3:
				car.deleteData();
				break;
			case 4:
				car.updateData();
				break;
			case 5:
			    System.out.println("Thank You");
			    break;
			default:
				System.out.println("Enter Valid Number");
			
			}
			}
			catch(NumberFormatException e) {
				System.out.println("Enter valid Command");
			}
		
		}while(ch!=5);
	}

	public void updateData() {
		CarInventory ci=new CarInventory();
		System.out.println("enter the car id you want to update");
		int id=sc.nextInt();
		System.out.println("Enter the Car Make");
		String make=sc.next();
		ci.setMake(make);
		System.out.println("Enter the Car model");
		String model=sc.next();
		ci.setModel(model);
		System.out.println("Enter the Car year ");
		int year=sc.nextInt();
		ci.setYear(year);
		System.out.println("Enter the Car price");
		double price=sc.nextDouble();
		ci.setPrice(price);
		try {
			Connection con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("update cars set make=?,model=?,price=?,year=? where car_id=?");
			ps.setString(1, ci.getMake());
			ps.setString(2, ci.getModel());
			ps.setDouble(3, ci.getPrice());
			ps.setInt(4,ci.getYear());
			ps.setInt(5, id);
			int x=ps.executeUpdate();
			
			if(x==1) {
				System.out.println("Updation Done");
			}
			else {
				System.out.println("No record found");
			}
			ps.close();
			con.close();
		}
		catch(Exception e) {
			
		}	
	}

	public void deleteData() {
		try{
			
			String model;
			System.out.println("Enter the Car name to delete a car data");
			model=sc.next();
			CarInventory ci=new CarInventory();
			ci.setModel(model);
			Connection con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("delete from Cars where model=?");
			ps.setString(1, ci.getModel());
			int x=ps.executeUpdate();
			if(x==1) {
				System.out.println("Car item deleted");
			}
			else {
				System.out.println("Record not found");
			}
			ps.close();
			con.close();

		}
		catch(Exception e){
			
		}
		
	}

	public void listData() {
		try {
			Connection con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from cars");
			ResultSet rs=ps.executeQuery();
			PreparedStatement ps1=con.prepareStatement("select count(*) from cars");
			ResultSet rs1=ps1.executeQuery();
			PreparedStatement ps2=con.prepareStatement("select sum(price) from cars");
			ResultSet rs2=ps2.executeQuery();
			if(rs1.next()) {
				int count=rs1.getInt(1);
				System.out.println("Total Number of cars are:"+" "+count);

			}
			if(rs2.next()) {
				int count1=rs2.getInt(1);
				System.out.println("Total Inventory($):"+" "+count1);

			}
			while(rs.next()) {
				System.out.println("\t"+rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4)+" "+"$"+rs.getDouble(5));	
			}
			rs.close();
			ps.close();
			con.close();
			}
		catch(Exception e) {
			
		}
	}
     public void addData() {
		CarInventory ci=new CarInventory();
		try {
		Connection con = DBConnection.getConnection();
		PreparedStatement ps1=con.prepareStatement("select count(*) from cars");
		ResultSet rs=ps1.executeQuery();
		if(rs.next()) {
			int count=rs.getInt(1);
			if(count>=20) {
				System.out.println("Directory full");
			}
			else {
				System.out.println("Enter Car Make");
				String make=sc.next();
				ci.setMake(make);
				System.out.println("Enter Car model");
				String model=sc.next();
				ci.setModel(model);
				System.out.println("Enter Car year");
				int year=sc.nextInt();
				ci.setYear(year);
				System.out.println("Enter Car Price");
				double price=sc.nextDouble();
				ci.setPrice(price);
				try {
					Connection con1 = DBConnection.getConnection();
					PreparedStatement ps=con1.prepareStatement("insert into cars(make,model,year,price)values(?,?,?,?)");
					ps.setString(1, ci.getMake());
					ps.setString(2, ci.getModel());
					ps.setInt(3, ci.getYear());
					ps.setDouble(4, ci.getPrice());
					int nr=ps.executeUpdate();
					ps.close();
					con.close();
					if(nr==1) {
						System.out.println("New Car details are added");
					}
				}
		        catch(Exception e) {
					System.out.println(e);
				}
				
			}

		}
		}
		catch(Exception e) {
			}
		}
		}


